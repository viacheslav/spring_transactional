package javaschool.dao;


import javaschool.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class PersonDao {

    @Autowired
    private DataSource dataSource;

    public void savePerson(Person person) {
        String insertPerson = "INSERT INTO person (name, age) VALUES (?,?)";

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(insertPerson, person.getName(),
                person.getAge());
        System.out.println("Created a person");
//        throw new RuntimeException();
    }
}
