package javaschool.service;


import javaschool.dao.PersonDao;
import javaschool.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    private PersonDao personDao;

    //    @Transactional
    public void savePerson(Person person) {
        personDao.savePerson(person);
    }

}
