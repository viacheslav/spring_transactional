package javaschool;

import javaschool.config.Config;
import javaschool.entity.Person;
import javaschool.service.PersonService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        PersonService service = context.getBean(PersonService.class);


        Person person = new Person();
        person.setName("Alex");
        person.setAge(35);

        service.savePerson(person);
    }
}
