### Test steps ###

1. Run the code as is. Check the new row in DB.
2. Uncomment exception throwing in PersonDao. Check the new row in DB.
3. Uncomment @EnableTransactionManagement in Config, @Transactional in PersonService.
See that new row is not created
4. Remove exception from PersonDao. Check new row in DB.
